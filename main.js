(function () {
    var json = JSON.parse(document.body.innerText);
    var jsonFormatted = JSON.stringify(json, null, '\t');

    document.body.innerHTML = '<div id="editor"/>';

    var editor = ace.edit("editor");
    // editor.setTheme("ace/theme/monokai");

    editor.session.setMode("ace/mode/json");
    editor.session.setUseWrapMode(true);
    editor.setReadOnly(true);
    editor.setShowPrintMargin(false);
    editor.setValue(jsonFormatted, -1);
    editor.focus();

    // Find programmatically:
    // var x = editor.find("eyeColor", {}, false);
    // editor.getSelection().setSelectionRange(x);
})();